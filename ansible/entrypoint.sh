#!/bin/bash

# Add local user
# Either use the LOCAL_USER_ID if passed in at runtime or
# fallback

USER_ID=${LOCAL_USER_ID:-9001}

echo "Starting with UID : $USER_ID"
useradd -s /bin/bash -u $USER_ID -o -M -d /home/$USER_NAME $USER_NAME
export HOME=/home/$USER_NAME

exec gosu $USER_NAME "$@"
