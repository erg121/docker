#!/bin/bash

# tailf /var/log/messages


td-agent --dry-run && td-agent -d /tmp/td.pid

sleep 10s;

cat /dev/urandom | tr -dc 'a-zA-Z0-9-!@#$%^&*()+{}|:<>?=' | fold -w 100 | pv -L $bw | nc localhost 20001
