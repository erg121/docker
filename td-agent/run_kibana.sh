#!/bin/bash
docker run --rm -it -p 5601:5601 --name kibana \
-v $(readlink -e kibana.yml):/usr/share/kibana/config/kibana.yml \
docker.elastic.co/kibana/kibana-oss:6.1.2
